Debian での hiki について
-------------------------

Debian では，簡単に hiki をセットアップするために，hikisetup コマンドを
用意しています．hiki を設置するディレクトリで hikisetup コマンドを実行
するだけで，hiki を動作させる環境ができあがります．

  ~/public_html/hiki% hikisetup --lang ja
  INFO: hiki basedir is /home/tach/public_html/hiki 
  Please input data_path [/home/tach/public_html/hiki/data]: 
  hikisetup succeeded!
  Please edit /home/tach/public_html/hiki/hikiconf.rb

hikisetup コマンドで設置した hiki は，パッケージをアップグレードした
際に，自動的に新しい hiki が動作するようになっています．そのため，
hiki を更新するときにはパッケージをアップグレードするだけで OK です．
しかし，hiki のメジャーアップグレードの際には設定ファイルの書式が
変更になる可能性がありますので，そのときは再度 hikisetup コマンドを
実行することをおすすめします．hikisetup コマンドは，何度実行しても
環境を壊さないように作成してあります．

Version 0.6.x 以前をお使いの方へ
--------------------------------

hiki は，Version 0.8 から設定ファイルの書式が変更になっています．
hikisetup コマンドでは，$data_path と $cgi_name のみ新しいファイル
に移行し，その他の設定はサンプルのものをそのまま利用し，hikiconf.rb
を生成します．古いファイルはリネームして残してありますので，手動で
再設定してください．なお，プラグインなどの書式も変わっていますので，
ご注意ください．詳しくは，/usr/share/doc/hiki/VERSIONUP.txt を参照
してください．

 -- Taku YASUI <tach@debian.or.jp>, Sat, 28 Jun 2003 11:01:05 +0900
